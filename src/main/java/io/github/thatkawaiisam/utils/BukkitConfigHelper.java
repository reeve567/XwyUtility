package io.github.thatkawaiisam.utils;

import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;

public class BukkitConfigHelper extends YamlConfiguration {

	/* File */
	private File file;
	/* Strings */
	private String name, directory;

	/**
	 * Bukkit Configuration Class
	 *
	 * @param name      - Is the identifier for the configuration file and object.
	 * @param directory - Directory.
	 */
	public BukkitConfigHelper(JavaPlugin plugin, String name, String directory) {
		/* Set the Name */
		setName(name);
		/* Set the Directory */
		setDirectory(directory);
		/* Set File */
		file = new File(directory, name + ".yml");
		/* If file does not already exist, then grab it internally from the resources folder */
		if (!file.exists()) {
			plugin.saveResource(name + ".yml", false);
		}
		load();
		save();
		// OLD YamlConfiguration.loadConfiguration(this.getFile());
	}

	public void load() {
		/* Load the files configuration */
		try {
			this.load(this.file);
		} catch (IOException | InvalidConfigurationException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Saves the configuration file from memory to storage
	 */
	public void save() {
		try {
			this.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Configuration getConfiguration() {
		return this;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDirectory(String directory) {
		this.directory = directory;
	}

	public String getDirectory() {
		return directory;
	}

	@Override
	public String getName() {
		return name;
	}
}
