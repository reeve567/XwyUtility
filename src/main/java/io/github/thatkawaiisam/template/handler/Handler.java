package io.github.thatkawaiisam.template.handler;

import io.github.thatkawaiisam.utils.BukkitConfigHelper;
import org.bukkit.plugin.java.JavaPlugin;
import pw.xwy.utils.logging.LogLevel;
import pw.xwy.utils.logging.LoggingHandler;

public abstract class Handler {

	private String handlerName;
	private BukkitConfigHelper configuration;

	private JavaPlugin instance;

	public Handler(String handlerName, boolean generateConfigurationFile, JavaPlugin instance) {
		this.instance = instance;
		this.handlerName = handlerName;

		//Whether or not to generate a configuration file.
		if (generateConfigurationFile) {
			configuration = new BukkitConfigHelper(
					instance,
					handlerName,
					instance.getDataFolder().getAbsolutePath()
			);
		}
		LoggingHandler.log(String.format("Registered %s handler", handlerName), LogLevel.INFO, false);
	}

	public String getHandlerName() {
		return handlerName;
	}

	public abstract void onEnable();

	public abstract void onDisable();

	public void enable() {
		onEnable();
		LoggingHandler.log(String.format("Enabled %s handler", handlerName), LogLevel.INFO, false);
	}

	public void disable() {
		onDisable();
		LoggingHandler.log(String.format("Disabled %s handler", handlerName), LogLevel.INFO, false);
	}

	public BukkitConfigHelper getConfiguration() {
		return configuration;
	}

	protected JavaPlugin getInstance() {
		return instance;
	}
}
