package io.github.thatkawaiisam.template.handler.impl;

import io.github.thatkawaiisam.template.handler.Handler;
import io.github.thatkawaiisam.utils.ClassUtility;
import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashSet;
import java.util.Set;

public class ListenerHandler extends Handler {

	private final Set<Listener> listeners = new HashSet<>();

	private ListenerHandler(JavaPlugin instance) {
		super("listeners", false, instance);
	}

	public ListenerHandler(String listenerPath, JavaPlugin instance) {
		this(instance);
		loadListenersFromPackage(instance, listenerPath);
	}

	public void loadListenersFromPackage(JavaPlugin plugin, String packageName) {
		ClassUtility.getClassesInPackage(plugin, packageName).stream()
				.filter(this::isListener)
				.forEach(aClass -> {
					try {
						listeners.add((Listener) aClass.getDeclaredConstructor(JavaPlugin.class).newInstance(plugin));
					} catch (Exception e) {
						e.printStackTrace();
					}
				});
	}

	public boolean isListener(Class<?> clazz) {
		for (Class<?> interfaze : clazz.getInterfaces()) {
			if (interfaze == Listener.class) {
				return true;
			}
		}

		return false;
	}

	public void registerListeners() {
		this.listeners.forEach(listener -> Bukkit.getPluginManager().registerEvents(listener, getInstance()));
	}

	public void unregisterListeners() {
		this.listeners.forEach(HandlerList::unregisterAll);
	}

	@Override
	public void onEnable() {
		registerListeners();
	}

	@Override
	public void onDisable() {
		unregisterListeners();
	}
}
