package io.github.thatkawaiisam.template.module;

import io.github.thatkawaiisam.utils.BukkitConfigHelper;
import org.bukkit.plugin.java.JavaPlugin;

public abstract class Module {

	private BukkitConfigHelper configuration;
	private String moduleName;
	private boolean enabled = false;

	private JavaPlugin instance;

	public Module(String moduleName, JavaPlugin instance, boolean generateConfiguration) {
		this.instance = instance;
		this.moduleName = moduleName;

		if (generateConfiguration) {
			//Setup configuration for each module
			this.configuration = new BukkitConfigHelper(
					instance,
					"modules/" + moduleName,
					instance.getDataFolder().getAbsolutePath()
			);
		}

		instance.getLogger().info(String.format("Successfully registered module '%s'.", moduleName));
	}

	public void enable() {
		onEnable();
		setEnabled(true);
		instance.getLogger().info(String.format("Successfully enabled module '%s'.", moduleName));
	}

	public void disable() {
		onDisable();
		setEnabled(false);
		instance.getLogger().info(String.format("Successfully disabled module '%s'.", moduleName));
	}

	public abstract void onEnable();

	public abstract void onDisable();

	public String getDisplayStatus() {
		return enabled ? "&aEnabled" : "&cDisabled";
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getModuleName() {
		return moduleName;
	}

	public BukkitConfigHelper getConfiguration() {
		return configuration;
	}

	public JavaPlugin getPlugin() {
		return instance;
	}
}
