package pw.xwy.utils;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.meta.SkullMeta;

public class PlayerSkull extends CustomItem {

	public PlayerSkull(Player p) {
		super(Material.PLAYER_HEAD);
		SkullMeta meta = (SkullMeta) getItemMeta();
		meta.setOwningPlayer(p);
		setItemMeta(meta);
	}

}