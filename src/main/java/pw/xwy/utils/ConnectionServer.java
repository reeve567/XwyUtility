package pw.xwy.utils;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public abstract class ConnectionServer implements Runnable, Server {
	
	private final int port;
	private ServerSocket serverSocket;
	private boolean running = false;
	
	public ConnectionServer(int port) {
		this.port = port;
	}
	
	@Override
	public void run() {
		start();
		System.out.println("Connection Server started on port " + port);
		nextClient();
	}
	
	@Override
	public void nextClient() {
		if (running) {
			try {
				Socket clientSocket = serverSocket.accept();
				handleClient(new Connection(clientSocket));
			} catch (IOException e) {
				e.printStackTrace();
			}
			nextClient();
		}
	}
	
	public abstract void handleClient(Connection socket);
	
	@Override
	public void start() {
		try {
			serverSocket = new ServerSocket(port);
			running = true;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void stop() {
		try {
			running = false;
			serverSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
