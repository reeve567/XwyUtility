package pw.xwy.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

public class Connection {
	
	private final String address;
	private final int port;
	private PrintWriter out;
	private BufferedReader in;
	private Socket socket;
	private ArrayList<String> results = new ArrayList<>();
	
	public Connection(int port) {
		address = "localhost";
		this.port = port;
		start();
	}
	
	public Connection(String address, int port) {
		this.address = address;
		this.port = port;
		start();
	}
	
	public Connection(Socket s) {
		socket = s;
		address = s.getInetAddress().toString().substring(1);
		port = s.getPort();
	}
	
	private void loadInputIfNeeded() {
		if (in == null) {
			try {
				in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void loadOutputIfNeeded() {
		if (out == null) {
			try {
				out = new PrintWriter(socket.getOutputStream());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void start() {
		try {
			socket = new Socket(address, port);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public ArrayList<String> checkResults() {
		loadInputIfNeeded();
		try {
			String line;
			while ((line = in.readLine()) != null) {
				results.add(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return results;
	}
	
	public void sendString(String s) {
		loadOutputIfNeeded();
		out.write(s + "\n");
		out.flush();
	}
	
	public void sendStrings(ArrayList<String> strings) {
		loadOutputIfNeeded();
		for (String s : strings) {
			sendString(s);
		}
	}
	
	public void stop() {
		try {
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
