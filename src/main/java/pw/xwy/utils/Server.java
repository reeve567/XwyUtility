package pw.xwy.utils;

interface Server {
	
	void nextClient();
	
	void start();
	
	void stop();
	
}
