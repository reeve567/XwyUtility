package pw.xwy.utils.logging;


import javafx.util.Pair;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class LoggingHandler {

	private static final List<Pair<String, Pair<LogLevel, Boolean>>> messages = new ArrayList<>();

	public LoggingHandler(JavaPlugin plugin) {
		Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, () -> {
			if (!messages.isEmpty()) {
				log(messages.get(0).getKey(), messages.get(0).getValue().getKey(), messages.get(0).getValue().getValue());
				messages.remove(0);
			}
		}, 0, 10);
	}

	static String getPrefix(LogLevel level) {
		Date date = new Date();
		String prefix = "[" + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds() + "] ";
		switch (level) {
			case NONE:
				prefix = "";
				break;
			case INFO:
				prefix += "[INFO] ";
				break;
			case ERROR:
				prefix += "[ERROR] ";
				break;
			case WARNING:
				prefix += "[WARNING] ";
				break;
			default:
				break;
		}
		return prefix;
	}

	public static void log(String message, LogLevel level, boolean outside) {
		System.out.println(getPrefix(level) + message);
		if (outside) {
			DiscordHandler.message(message, level);
			SlackHandler.message(message, level);
		}
	}

	public static void addLog(String message, LogLevel level, boolean outside) {
		messages.add(new Pair<>(message, new Pair<>(level, outside)));
	}

}
