package pw.xwy.utils.logging;

public enum LogLevel {
	INFO, ERROR, WARNING, NONE
}
