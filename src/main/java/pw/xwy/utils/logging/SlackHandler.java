package pw.xwy.utils.logging;

import com.github.seratch.jslack.Slack;
import com.github.seratch.jslack.api.webhook.Payload;

import java.io.IOException;

public class SlackHandler {
	
	private static boolean enabled;
	private static String webhook;
	private static Slack slack;

	public static void setup(boolean enabled, String webhook) {
		SlackHandler.enabled = enabled;

		if (slack == null && enabled) {
			slack = Slack.getInstance();

			SlackHandler.webhook = webhook;
		}
	}

	public static void message(String message) {
		message(message, LogLevel.INFO);
	}

	public static void message(String message, LogLevel level) {
		if (!enabled)
			return;

		String prefix = LoggingHandler.getPrefix(level);
		Payload message1 = Payload.builder()
				.text(prefix + message)
				.build();
		try {
			slack.send(webhook, message1);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
