package pw.xwy.utils.logging;

import com.mrpowergamerbr.temmiewebhook.DiscordMessage;
import com.mrpowergamerbr.temmiewebhook.TemmieWebhook;

public class DiscordHandler {
	
	private static boolean enabled;
	private static String title;
	private static String icon;
	private static TemmieWebhook temmie;

	public static void setup(boolean enabled, String webhook, String title, String icon) {
		DiscordHandler.enabled = enabled;

		if (temmie == null && enabled) {
			temmie = new TemmieWebhook(webhook);
			DiscordHandler.title = title;
			DiscordHandler.icon = icon;
		}
	}

	public static void message(String message) {
		message(message, LogLevel.INFO);
	}
	
	public static void message(String message, LogLevel logLevel) {
		if (!enabled)
			return;

		String prefix = LoggingHandler.getPrefix(logLevel);
		DiscordMessage message1 = new DiscordMessage(title, prefix + message, icon);
		temmie.sendMessage(message1);
	}
	
}
