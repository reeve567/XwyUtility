package pw.xwy.utils;

import java.util.ArrayList;
import java.util.Arrays;

public class test {
	
	public static void main(String[] args) {
		new Thread(new ConnectionServer(5202) {
			@Override
			public void handleClient(Connection socket) {
				System.out.println(socket.checkResults());
				socket.stop();
			}
		}).start();
		Connection connection = new Connection(5202);
		connection.sendStrings(new ArrayList<>(Arrays.asList("test1", "test2")));
		connection.stop();
	}
	
}
