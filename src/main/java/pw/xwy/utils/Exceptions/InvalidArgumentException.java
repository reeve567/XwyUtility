package pw.xwy.utils.Exceptions;

public class InvalidArgumentException extends XwyException {
	
	public InvalidArgumentException(String argument, String reason) {
		super("Invalid argument '" + argument + "': " + reason);
	}
	
	public InvalidArgumentException(String s) {
		super(s);
	}
}
