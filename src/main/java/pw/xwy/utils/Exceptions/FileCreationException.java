package pw.xwy.utils.Exceptions;

public class FileCreationException extends XwyException {
	
	public FileCreationException(String name, String reason) {
		super(name + " : " + reason);
	}
	
}
