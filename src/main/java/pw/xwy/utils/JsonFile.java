package pw.xwy.utils;

import org.bukkit.plugin.java.JavaPlugin;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import pw.xwy.utils.logging.LogLevel;
import pw.xwy.utils.logging.LoggingHandler;

import java.io.*;

public abstract class JsonFile {

	private JSONObject body;
	private long version;
	private boolean dynamic;
	private String file;

	public JsonFile(String file, boolean resourceAsStream, long version, boolean dynamic) {
		this.version = version;
		this.dynamic = dynamic;
		this.file = file;
		Reader reader = getReader(file, resourceAsStream);
		if (reader != null) {
			JSONParser parser = new JSONParser();
			try {
				body = (JSONObject) parser.parse(reader);
			} catch (IOException | ParseException e) {
				LoggingHandler.log("IOException or ParseException: \nFile: " + file + "\nresourceAsStream: " + resourceAsStream, LogLevel.ERROR, true);
			}
		} else {
			try {
				throw new FileNotFoundException();
			} catch (FileNotFoundException e) {
				LoggingHandler.log("JSON file not found or something! I think! \nFile: " + file + "\nresourceAsStream: " + resourceAsStream, LogLevel.ERROR, true);
			}
		}

		if (body.containsKey("version")) {
			if ((long) body.get("version") == version) {
				load();
			} else if (dynamic) {
				setDefaults();
				save();
			}
		}

	}

	private Reader getReader(String file, boolean resourceAsStream) {
		Reader reader = null;

		if (resourceAsStream) {
			reader = new InputStreamReader(JsonFile.class.getResourceAsStream(file));
		} else {
			try {
				reader = new FileReader(new File(file));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
		return reader;
	}

	public JSONObject getBody() {
		return body;
	}

	public long getVersion() {
		return version;
	}

	public boolean isDynamic() {
		return dynamic;
	}

	public abstract void load();

	public void save() {
		try (FileWriter writer = new FileWriter(file)) {
			writer.write(body.toJSONString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void setDefaults() {

	}
}