package pw.xwy.utils;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CustomItem extends ItemStack {
	
	public CustomItem(Material material) {
		super(material);
	}
	
	public CustomItem(ItemStack stack) {
		super(stack);
	}
	
	private ItemMeta meta() {
		return getItemMeta();
	}
	
	public CustomItem setMeta(ItemMeta meta) {
		setItemMeta(meta);
		return this;
	}
	
	public CustomItem setCusomType(Material material) {
		setType(material);
		return this;
	}
	
	public CustomItem addEnchant(Enchantment enchantment, int level) {
		addUnsafeEnchantment(enchantment, level);
		return this;
	}
	
	public CustomItem setCustomAmount(int amount) {
		super.setAmount(amount);
		return this;
	}
	
	public CustomItem addLore(String s) {
		ItemMeta meta = meta();
		List<String> lores = new ArrayList<>();
		if (meta.hasLore()) {
			lores = meta.getLore();
		}
		if (s.split("\n").length > 1) {
			for (String st : s.split("\n")) {
				lores.add(ChatColor.translateAlternateColorCodes('&', st));
			}
		} else {
			lores.add(ChatColor.translateAlternateColorCodes('&', s));
		}
		meta.setLore(lores);
		setMeta(meta);
		return this;
	}
	
	public CustomItem setCustomLore(String... lore) {
		return setCustomLore(Arrays.asList(lore));
	}
	
	public CustomItem setCustomLore(List<String> lore) {
		ItemMeta meta = meta();
		List<String> lores = new ArrayList<>();
		for (String s : lore) {
			if (!s.equals("")) {
				lores.add(ChatColor.translateAlternateColorCodes('&', s));
			}
		}
		meta.setLore(lores);
		setMeta(meta);
		return this;
	}
	
	public CustomItem setLore(String s) {
		ItemMeta meta = meta();
		List<String> lores = new ArrayList<>();
		lores.add(ChatColor.translateAlternateColorCodes('&', s));
		meta.setLore(lores);
		setMeta(meta);
		return this;
	}
	
	public CustomItem setName(String s) {
		ItemMeta meta = meta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', s));
		setMeta(meta);
		return this;
	}
	
	@Override
	public CustomItem clone() {
		return (CustomItem) super.clone();
	}
}