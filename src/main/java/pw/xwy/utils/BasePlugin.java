package pw.xwy.utils;

import io.github.thatkawaiisam.template.handler.Handler;
import io.github.thatkawaiisam.template.handler.impl.*;
import io.github.thatkawaiisam.template.module.Module;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

public abstract class BasePlugin extends JavaPlugin {

	private static BasePlugin instance;
	private List<Handler> handlers = new ArrayList<>();

	public BasePlugin() {
		instance = this;
	}

	public static BasePlugin getInstance() {
		return instance;
	}

	public Handler getHandler(String handlerName) {
		return handlers.stream()
				.filter(handler -> handler.getHandlerName().equalsIgnoreCase(handlerName))
				.findFirst()
				.orElse(null);
	}

	public Module getModule(String module) {
		return ((ModuleHandler) getHandler("modules")).getModule(module);
	}

	public void loadHanders(String modulePath, String commandPath, String listenerPath) {
		handlers.add(new ModuleHandler(modulePath, this, true));
		handlers.add(new CommandHandler(commandPath, this));
		handlers.add(new ConfigHandler(this));
		handlers.add(new LanguageHandler(this));
		handlers.add(new ListenerHandler(listenerPath, this));
	}

	public void enableHandlers() {
		for (Handler h : handlers) {
			h.enable();
		}
	}

	public void unloadHandlers() {
		for (Handler h : handlers) {
			h.disable();
		}
	}
}
